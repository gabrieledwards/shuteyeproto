﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{
    public float speed = 100;
    public Rigidbody2D rb;
    public Text lifeText;
    public GameObject newEye;
    public GameObject newEye2;
    public Sprite secondBoy;
    public Sprite thirdBoy;

    private int lifeCount;
    
//
    void Start()
    {
        lifeCount = 1;
        SetCountText();
        newEye.SetActive(false);
        newEye2.SetActive(false);
     
        
    }

    public void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector3 tempVect = new Vector3(h, v, 0);
        tempVect = tempVect.normalized * speed * Time.deltaTime;
        rb.MovePosition(rb.transform.position + tempVect);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Life"))
        {
            Destroy(other.gameObject);
            lifeCount = lifeCount + 1;
            SetCountText();
        }
        else if (other.gameObject.CompareTag("Eye"))
        {
            Destroy(other.gameObject);
            newEye.SetActive(true);
            this.GetComponent<SpriteRenderer>().sprite = secondBoy;
        
        }
        else if (other.gameObject.CompareTag("Eye2"))
        {
            Destroy(other.gameObject);
            newEye2.SetActive(true);
            this.GetComponent<SpriteRenderer>().sprite = thirdBoy;
         
        }
        
    }

    void SetCountText()
    {
        lifeText.text = lifeCount.ToString();
    }
    

}
